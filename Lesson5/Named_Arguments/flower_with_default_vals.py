#Определить функцию flower_with_default_vals . Принимает 3 аргумента: цветок (по умолчанию
#“ромашка”), цвет (по умолчанию “белый”) и цена (по умолчанию 10.25). Функция
#flower_with_default_vals выводит строку в формате “Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>”.
#При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки (*
#Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000. В
#функции main вызвать функцию flower_with_default_vals различными способами (перебрать
#варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена). (* Использовать
#именованные аргументы).


def flower_with_default_vals(flower ='chamomile', colour = 'white', price = 10.25):
    if type(flower) == str and type(colour) == str and (10000 > price > 0):
        return print(f'Цветок: {flower} | Цвет: {colour} | Цена: {price}')

    return print('введи строку')

def main():
    flower_with_default_vals()
    flower_with_default_vals(flower='rose', colour='blue', price=100)
    flower_with_default_vals(colour='green', price=1)
    flower_with_default_vals(flower="superflower", colour='black')

if __name__ == '__main__':
        main()
