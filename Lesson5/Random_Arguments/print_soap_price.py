#Определить функцию print_soap_price . Принимает 2 аргумента: название мыла (строка) и
#неопределенное количество цен (*args). Функция print_soap_price выводит вначале название мыла, а
#потом все цены на него. В функции main вызывается функция print_soap_price и передается название
#мяла и произвольное количество цен. Например, print_soap_price(‘Dove’, 10, 50) или
#print_soap_price(‘Мылко’, 456, 876, 555).



def print_soap_price(name,*args):
    return print(name, *args)




def main():

    print_soap_price("Dove", 10, 50, 100)




if __name__ == '__main__':
    main()