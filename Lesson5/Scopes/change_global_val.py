#В глобальной области видимости определена переменная my_var с каким-то значением. В функции
#main выводим значение my_var на экран. В функции change_global_val меняем значение глобальной
#переменной my_var на другое. После этого в функции main опять выводим значение my_var на экран

my_var = 1

def change_global_val():
   global my_var
   my_var = 2333





def main():
    print(my_var)
    change_global_val()
    print(my_var)



if __name__ == '__main__':
        main()