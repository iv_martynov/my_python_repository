#Функция convert_list_to_tuple . Принимает 2 аргумента: список (с повторяющимися элементами) и
#значение для поиска. Возвращает кортеж (из входного списка) и найдено ли искомое значение(True |
#False).

def convert_list_to_tuple(my_list, key):
    my_tuple = tuple(my_list)
    vals_in_tuple = True if key in my_list else False
    return my_tuple , vals_in_tuple



def main():
    my_list = [1, 2, 'ann', 1, 'ann', 'kate']
    key = 122
    print(convert_list_to_tuple(my_list,key))

if __name__ == '__main__':
    main()