#Функция gen_list_double . Принимает число n. Возвращает список длиной n, состоящий из удвоенных
#значений от 0 до n. Пример: n=3, результат [0, 2, 4].

def gen_list_double(n):
    double = []
    for n in range(0,n):
        double.append(n * 2)
    return double

def main():
    print(gen_list_double(n=10))


if __name__ == '__main__':
    main()