#Функция zip_names . Принимает 2 аргумента: список с именами и множество с фамилиями.
#Возвращает список с парами значений из каждого аргумента. (* Провести эксперименты с различными
#длинами входных данных. Если они одинаковые? А если разные? А если один из них пустой?)


def zip_names(fnames_list, lnames_set):
    return zip(fnames_list,lnames_set)



def main():
    fnames_list = ['Ivan', 'Petr', 'Sidr']
    lnames_set = {'Ivanov', 'Petrov', 'Sidorov'}
    print(list(zip_names(fnames_list,lnames_set)))

if __name__ == '__main__':
    main()