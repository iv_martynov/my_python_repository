#Функция triple_sting_print_len . Принимает строку. Вывести ее три раза через запятую и показать количество символов в ней

def triple_sting_print_len(n):

    print (n, n, n, sep=',')
    print(len(n+n+n))


if __name__ == '__main__':
    n = input("Введите строку")
    triple_sting_print_len(n)