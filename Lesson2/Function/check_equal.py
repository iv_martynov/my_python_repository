#Функция check_equal Принимает 3 числа. Вернуть True, если среди них есть одинаковые, иначе False.



def check_equal(x,y,z):

    if x == y:
        return True
    elif x == z:
        return True
    elif y == z:
        return True
    else:
        return False


if __name__ == '__main__':

    x = int(input("введите число 1"))
    y = int(input("введите число 2"))
    z = int(input("введите число 3"))

    print(check_equal(x, y, z))