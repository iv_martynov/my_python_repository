#Функция get_translation_by_word . Принимает 2 аргумента: ru-eng словарь содержащий ru_word:
#[eng_1, eng_2 ...] и слово для поиска в словаре (ru). Возвращает все варианты переводов, если такое
#слово есть в словаре, если нет, то ‘Can’t find Russian word: {word}’.


def get_translation_by_word(ru_word,ru):
    for val, k in ru_word.items():
        if ru in val:
            return ru_word.get(ru)
    return f'Can not find Russian word:' + ru


def main():
    ru_word = {'привет': ['hello', 'hi', 'howdoyoudo'], 'пока':['bye','seeya'] ,'спасибо':['thanks']}
    ru = 'спасибо'
    print(get_translation_by_word(ru_word,ru))

if __name__ == '__main__':
    main()