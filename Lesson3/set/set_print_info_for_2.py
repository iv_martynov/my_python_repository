# Функция set_print_info_for_2 . Принимает 2 аргумента: множества “ассорти” my_set_left и my_set_left
# (которые создал пользователь). Выводит информацию о: равенстве множеств, имеют ли они общие
# элементы, является ли my_set_left подмножеством my_set_right и наоборот.


def set_print_info_for_2(my_set_left, my_set_right):
    print(f'left:{my_set_left} right:{my_set_right}')
    rv = (my_set_right == my_set_left)
    print(f'равенство?{rv}')
    sub1 = my_set_left.issubset(my_set_right)
    sub2 = my_set_right.issubset(my_set_left)
    print(f'подмножество1{sub1}, подмножество2 {sub2}')
    se1 = my_set_left.intersection(my_set_right)
    se2 = my_set_right.intersection(my_set_left)
    print(f'равенство1{se1},{se2}')


def main():
    user_str_left = input("Введите множества через запятую")
    user_str_right = input("Введите множества через запятую")
    user_set_left = set(user_str_left.split(','))
    user_set_right = set(user_str_right.split(','))
    set_print_info_for_2(user_set_left, user_set_right)

if __name__ == '__main__':
        main()
