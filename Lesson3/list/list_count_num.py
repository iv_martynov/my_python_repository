#Функция list_count_num . Принимает 2 аргумента: список c числами my_list (который создал
# пользователь) и число num. Возвращает количество num в списке my_list.

def list__count_num(my_list, num):
    return my_list.count(num)

if __name__ == '__main__':
        my_list = str(input('введите список с числами через пробел'))
        my_list = my_list.split(' ')
        num = input("введите число")
print(list__count_num(my_list,num))