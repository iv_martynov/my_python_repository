#Функция list_count_word . Принимает 2 аргумента: список слов my_list (который создал пользователь)
#и строку word. Возвращает количество word в списке my_list.

def list_count_word(my_list, word):
    return my_list.count(word)

if __name__ == '__main__':
    _str = input("Ведите слова через запятую")
    my_list = _str.split(',')
    word = input("Ведите слово")

print(list_count_word(my_list,word))