#Функция tuple_len_first . Принимает 1 аргумент: кортеж “ассорти” my_tuple (который создал
#пользователь). Возвращает кортеж состоящий из длины кортежа и первого элемента кортежа.
#Пример: (‘55’, ‘aa’, 66), результат (3, ‘55’).

def tuple_len_first(my_tuple):
    return len(my_tuple), my_tuple[0]

if __name__ == '__main__':
    _str = input("введите кортеж ассорти")
    my_tuple = tuple(_str.split(' '))

    print(tuple_len_first(my_tuple))
