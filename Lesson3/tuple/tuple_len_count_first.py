# Функция tuple_len_count_first . Принимает 2 аргумента: кортеж “ассорти” my_tuple (который создал
# пользователь) и число num. Возвращает кортеж состоящий из длины кортежа, количества чисел num
# в кортеже my tuple и первого элемента кортежа. Пример: my_tuple=(‘55’, ‘aa’, 66) num = 66, результат
# (3, 1, ‘55’)

def tuple_len_count_first(my_tuple, num):
    return len(my_tuple), my_tuple.count(num), my_tuple[0]


if __name__ == '__main__':
    my_tuple = tuple((input('введите кортеж через пробелы')).split(' '))
    num = input('введите число')

    print(tuple_len_count_first(my_tuple,num))
