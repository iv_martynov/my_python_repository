#Класс OddIntIterator . Аргументы: целое число n . Перегрузить методы __init__ , __iter__ и __next__
#таким образом чтобы при итерировании по экземпляру OddIntIterator метод next возвращает
#следующее нечетное число в диапазоне [0, n ), если произошел выход за пределы этого диапазона -
#ошибка StopIteration .

class OddIntIterator:
    def __init__(self, n):
        return n
 