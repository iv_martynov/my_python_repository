#Класс Human . Атрибуты age , first_name, last_name . При создании экземпляра инициализировать
#атрибуты класса. Создать метод get_age , который возвращает возраст человека. Перегрузить
#оператор __eq__ , который сравнивает объект человека с другим по атрибутам. Перегрузить оператор
#__str__ , который возвращает строку в виде “Имя: first_name last_name Возраст: age ”.


class Human(object):
    def __init__(self, age, first_name, last_name):
        self.age = age
        self.first_name = first_name
        self.last_name = last_name

    """def __eq__(self, human2):
        if self.__dict__ == human2.__dict__:
           return print(True)
        else:
           return print(False)"""

    def __eq__(self, human2):
        if isinstance(human2, Human):
            return(print(self.age == human2.age and
                    self.first_name == human2.first_name and
                       self.last_name == human2.last_name))
        return print(False)

    def get_age(self):
        return print(self.age)

    def __str__(self):
        return print(f'Имя: {self.first_name} {self.last_name} Возраст: {self.age}')



def main():

    human1 = Human(18, "Ivan", 'Ivanov')
    human1.get_age()
    human1.__str__()
    human2 = Human(18, "Ivan", 'Ivanov')
    human2.get_age()
    human2.__str__()
    human1.__eq__(human2)

if __name__ == '__main__':
        main()
