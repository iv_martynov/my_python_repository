#Создать класс Zoo . Атрибуты animal_count (количество животных), name и money (сколько денег на
#счету). При создании экземпляра инициализировать атрибуты класса. Создать метод
#get_animal_count , который возвращает количество животных animal_count в зоопарке. Создать
#метод print_name , который печатает строку “ name - это лучший зоопарк!”. Создать метод can_afford ,
#который принимает аргумент needed_money и возвращает True если зоопарк может заплатить
#столько денег (self.money >= needed_money), иначе False

class Zoo:
    def __init__(self, animal_count, name, money):
        self.animal_count = animal_count
        self.name = name
        self.money = money

    def get_animal_count(self):
        return print(self.animal_count)

    def print_name(self):
        return print(f'{self.name} - это лучший зоопарк')

    def can_afford(self, needed_money):
        if self.money >= needed_money:
            return print(True)
        print(False)

def main():
    zoo1 = Zoo(45, 'SPB', 10000)
    zoo1.get_animal_count()
    zoo1.print_name()
    zoo1.can_afford(9000)
    zoo2 = Zoo(100,'Moscow', 15000)
    zoo2.get_animal_count()
    zoo2.print_name()
    zoo2.can_afford(17000)

if __name__ == '__main__':
    main()