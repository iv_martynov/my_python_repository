# Класс Dog . Атрибуты age , name . При создании экземпляра инициализировать атрибуты класса.
# Создать метод print_bark , который печатает “Гав-гав-гав. Это мое имя name на моем собачьем
# языке!”. Создать метод get_age , который возвращает значение атрибута age.


class Dog:

    def __init__(self, age, name):
        self.age = age
        self.name = name

    def print_bark(self):
        print("Гав-гав-гав. Это мое имя на моем собачьем языке!")

    def get_age(self):
        return print(self.age)


def main():
    dog1 = Dog(18, "Кристин")
    dog1.get_age()
    dog1.print_bark()


if __name__ == '__main__':
    main()
