#Функция even_generator_til_n . Параметры: целое число n. Генератор возвращает все четные числа в
#диапазоне [0, n). Использовать этот генератор в цикле for и отдельно с помощью функции next
#(проверить, что после исчерпания элементов возвращается ошибка).


def even_generator_til_n(n):
    for i in range(0, n):
        if i % 2 == 0:
            yield i


def main():
    myrange = even_generator_til_n(5)
    #print(list(myrange))
    print(next(myrange))
    print(next(myrange))
    print(next(myrange))
    print(next(myrange))

if __name__ == '__main__':
    main()